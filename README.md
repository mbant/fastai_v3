## Fast.ai notebooks and code

This repo contains most of the code/IPython notebooks written as I followed the [fast.ai "Practical DL for coders" course](https://course.fast.ai/). 

Some were run locally, but most were run using the amazing [Google Colaboratory](https://colab.research.google.com) environment so you might find some non-jupyter commands in there.

I mostly try to deviate (slightly, but also substancially from time to time) to experiment and make the most out of the course, so if you want to recover the original notebooks I'd suggest going [directly at the source](https://github.com/fastai/course-v3/tree/master/nbs).