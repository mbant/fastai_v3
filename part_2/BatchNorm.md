# BatchNorm and Backpropagation

While traversing lesson 10 I felt like diving a bit deeper on how BatchNorm works, so other than reading careully [the original paper](https://arxiv.org/abs/1502.03167) I **HAD TO** derive the backward pass by myself, also to understand the uses of `torch.no_grad()` and `.detach_()` throughout the notebook. Note that, as explained in the comments [in the notebook itself](07_batchnorm.ipynb), I think that the first use of it was actually a mistake, since the stats for the current batch were computed inside the `update_stats` functions. Regardless, here is what I learned ...

The forward pass of BatchNorm is computed as follows; consider the input of the batchnorm layer $\mathbf x$ (resulting from whatever previous module) with dimensions (in PyTorch) $B \times D$. I'm considering here a fully connected layer, in the case of a ConvNet like in our notebook, substitute $D \rightarrow H,W$ with the height and width of an image (and consider adding $C$, the channels/filters as well).

We first normalize the input by subtracting its mean and variance over the batch for all $D$ dimensions, resulting in tensors $\mu$ and $\sigma^2$ of dimension $D$.

$$\mu_d = \frac{1}{B} \sum \limits_{b=1}^B x_{bd}$$
$$\sigma^2_d = \frac{1}{B} \sum \limits_{b=1}^B (x_{bd} - \mu_d)^2$$

Now we normalize the inputs:

$$\hat x_{bd} = (x_{bd} - \mu_d)(\sigma_d^2+\varepsilon)^{-1/2}$$

where $\varepsilon$ is a small constant to ensure numerical stability. Finally we introduce two more learning parameters $\gamma$ and $\beta$, rigorously using greek letters, to generalize the transformation and ensuring the batch norm layer can recover the identity function:

$$y_{bd} = \hat x_{bd} \gamma_d + \beta_d$$

Now that the forward pass is clear, our aim is to compute $\frac{\partial \mathcal{L}}{\partial \theta}$ where $\mathcal{L}$ is the loss function and $\theta$ is **any** parameter / activation of our Batch Normalization module!

Luckily for us, the chain rule helps by allowing us to delegate everything *after* the batchnorm layer to the upper modules that will provide for us, during the backward pass, the quantity $\frac{\partial \mathcal{L}}{\partial y_{bd}}$. We can thus rewrite our objective as 

$$\frac{\partial \mathcal{L}}{\partial x_{ij}} = \sum \limits_{b,d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \times \frac{\partial y_{bd}}{\partial \theta}$$

The sum $\sum \limits_{b,d}$ is necessary, as the gradients may flow from $\mathcal{L}$ to $\theta$ from any of the $y_{bd}$ and we need to accumulate them. 

As a warm-up, let's consider first the easier derivatives with respect to the two learned parameters $\gamma$ and $\beta$ !

$$\frac{\partial \mathcal{L}}{\partial \beta_{j}} = \sum \limits_{b,d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \times \frac{\partial y_{bd}}{\partial \beta_j} = \sum \limits_{b,d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \times \frac{\partial \left(\hat x_{bd} \gamma_d + \beta_d\right)}{\partial \beta_j}$$

Now of course if $d\neq j$ in the sum, the inner derivative is zero; otherwise the derivative of $\hat x_{bd} \gamma_d + \beta_d$ wrt $\beta_d$ is simply one. This simplifies then to:

$$ \frac{\partial \mathcal{L}}{\partial \beta_{j}} = \sum \limits_{b} \frac{\partial \mathcal{L}}{\partial y_{bj}}$$

For $\gamma$ the same reasoning applies, albeit with a slightly more complex derivative:

$$\frac{\partial \mathcal{L}}{\partial \gamma_{j}} = \sum \limits_{b,d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \times \frac{\partial y_{bd}}{\partial \gamma_j} = \sum \limits_{b,d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \times \frac{\partial \left(\hat x_{bd} \gamma_d + \beta_d\right)}{\partial \gamma_j} = \sum \limits_{b} \frac{\partial \mathcal{L}}{\partial y_{bj}} \hat x_{bj} =$$
$$\sum \limits_{b} \frac{\partial \mathcal{L}}{\partial y_{bj}} (x_{bj} - \mu_j)(\sigma_j^2+\varepsilon)^{-1/2}$$

where the last step is just for clarity.


Even simpler is 

$$\frac{\partial \mathcal{L}}{\partial \hat x_{bd}} = \frac{\partial \mathcal{L}}{\partial y_{bd}} \times \frac{\partial y_{bd}}{\partial \hat x_{bd}} = \frac{\partial \mathcal{L}}{\partial y_{bd}} \gamma_d$$

since here each $y_{bd}$ depends only on exactly $\hat x_{bd}$ and thus there's no reason to expand the sum at all. If we wanted to be pedantic though (better do it now where the derivations are simpler!) we should have written

$$\frac{\partial \mathcal{L}}{\partial \hat x_{ij}} = \sum \limits_{b,d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \times \frac{\partial y_{bd}}{\partial \hat x_{ij}} = \sum \limits_{b,d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \times \frac{\partial \hat x_{bd} \gamma_d + \beta_d}{\partial \hat x_{ij}} =$$
$$\sum \limits_{b,d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \times \gamma_d \times \delta(b,i) \delta(d,j) = \frac{\partial \mathcal{L}}{\partial y_{ij}} \gamma_j$$

Where I introduced the indicator function $\delta(i,j)$ that is equal to $1$ only if $i=j$ and is equal to $0$ otherwise.

Be sure to understand all these details before moving forward as the derivative themselves are quite easy to compute ([Wolfram Alpha](www.wolframalpha.com) can do them, it's just applying some rules!) but it's necessary to understand the "dependency graph" between all the quantities in play to properly derive the backpropagation through a NN! This graph is very close to what PyTorch or TensorFlow stores when you ask it to do the backward pass by itself, so if you want to **understand** how these systems work you need to be able to follow here!

Let's try to compute now the more tricky bits!

$$\frac{\partial \mathcal{L}}{\partial x_{ij}} = \sum \limits_{l,m} \frac{\partial \mathcal{L}}{\partial y_{lm}} \times \frac{\partial y_{lm}}{\partial x_{ij}}$$

Note that here is very important to use two different set of indeces for $y_{bd}$ and $x_{ij}$ because, as we'll see, $y_{bd}$ depends on more than one element of $\mathbf x$ and we want to avoid confusion.

We can of course make use of the  intermediate result we computed above and write

$$\frac{\partial \mathcal{L}}{\partial x_{ij}} = \sum \limits_{l,m} \frac{\partial \mathcal{L}}{\partial y_{lm}} \times \frac{\partial y_{lm}}{\partial x_{ij}} =\sum \limits_{b,d} \sum \limits_{l,m} \frac{\partial \mathcal{L}}{\partial y_{lm}} \times \frac{\partial y_{lm}}{\partial \hat x_{bd}} \times \frac{\partial \hat x_{bd}}{\partial x_{ij}} =$$
$$\sum \limits_{b,d} \sum \limits_{l,m} \frac{\partial \mathcal{L}}{\partial y_{lm}} \times \Big( \gamma_m \times \delta(l,b) \delta(m,d) \Big) \times \frac{\partial \hat x_{bd}}{\partial x_{ij}} =$$
$$\sum \limits_{b,d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \gamma_d \frac{\partial \hat x_{bd}}{\partial x_{ij}}$$

The above is quite tricky the first time you look at it, but it's important as we're left with just one summation now!

Recall that $\hat x_{bd} = (x_{bd} - \mu_d)(\sigma_d^2+\varepsilon)^{-1/2}$ and let's focus on $\frac{\partial \hat x_{bd}}{\partial x_{ij}}$ for now to alleviate a bit the notation overhead. Using the product rule we can write:

$$\frac{\partial \hat x_{bd}}{\partial x_{ij}} = \frac{\partial (x_{bd} - \mu_d)(\sigma_d^2+\varepsilon)^{-1/2}}{\partial x_{ij}} = \frac{\partial (x_{bd} - \mu_d)}{\partial x_{ij}} \times (\sigma_d^2+\varepsilon)^{-1/2} + (x_{bd} - \mu_d) \times \frac{\partial (\sigma_d^2+\varepsilon)^{-1/2}}{\partial x_{ij}}$$

where I'm trying to be as explicit as I can. 

We can solve each piece separately, so for the first term
$$\frac{\partial (x_{bd} - \mu_d)}{\partial x_{ij}} = \frac{\partial \left(x_{bd} - \left( \frac{1}{B} \sum \limits_{b=1}^B x_{bd}\right)\right)}{\partial x_{ij}}$$

we can see that $\frac{\partial x_{bd}}{\partial x_{ij}}$ is equal to $1$ if and only if both $b=i$ and $d=j$ and zero everywhere else and similarly
$$\frac{\partial \left( \frac{1}{B} \sum \limits_{b=1}^B x_{bd}\right)}{\partial x_{ij}} = \frac{1}{B}$$
if and only if $d=j$ and zero everywhere else.

Putting this all toghether we can now write $$\frac{\partial (x_{bd} - \mu_d)}{\partial x_{ij}} = \delta(b,i)\delta(d,j) - \frac{1}{B} \delta(d,j)$$

Note that we could also write $$\frac{\partial (x_{bd} - \mu_d)}{\partial x_{ij}} = \frac{\partial x_{bd}}{\partial x_{ij}} - \frac{\partial \mu_d}{\partial x_{ij}} = \delta(b,i)\delta(d,j) - \frac{\partial \mu_d}{\partial x_{ij}}$$

and keep everything a little more general / implicit, which is the approach taken in the paper.

Similarly we have that 

$$\frac{\partial (\sigma_d^2+\varepsilon)^{-1/2}}{\partial x_{ij}} = -\frac{1}{2}(\sigma_d^2+\varepsilon)^{-3/2}\frac{\partial \sigma_d^2}{\partial x_{ij}} = -\frac{1}{2}(\sigma_d^2+\varepsilon)^{-3/2}\frac{\partial \left( \frac{1}{B} \sum \limits_{b=1}^B (x_{bd} - \mu_d)^2 \right)}{\partial x_{ij}} =$$
$$-\frac{1}{2}(\sigma_d^2+\varepsilon)^{-3/2} \frac{1}{B} \sum \limits_{b=1}^B \frac{\partial \left( x_{bd} - \mu_d \right)^2}{\partial x_{ij}} =$$
$$-\frac{1}{B}(\sigma_d^2+\varepsilon)^{-3/2} \sum \limits_{b=1}^B ( x_{bd} - \mu_d ) \left( \frac{\partial (x_{bd} - \mu_d) }{\partial x_{ij}}\right) =$$
$$-\frac{1}{B}(\sigma_d^2+\varepsilon)^{-3/2} \sum \limits_{b=1}^B ( x_{bd} - \mu_d ) \left( \delta(b,i)\delta(d,j) - \frac{1}{B} \delta(d,j) \right)$$

where we made use of the result obtained when taking the gradient of $(x_{bd} - \mu_d)$ just above.

We can further simplify this into 

$$\frac{\partial (\sigma_d^2+\varepsilon)^{-1/2}}{\partial x_{ij}} = -\frac{1}{B}(\sigma_d^2+\varepsilon)^{-3/2} \sum \limits_{b=1}^B ( x_{bd} - \mu_d ) \left( \delta(b,i)\delta(d,j) - \frac{1}{B} \delta(d,j) \right) = $$
$$-\frac{1}{B}(\sigma_d^2+\varepsilon)^{-3/2} \left( \sum \limits_{b=1}^B ( x_{bd} - \mu_d ) \delta(b,i)\delta(d,j) - \sum \limits_{b=1}^B ( x_{bd} - \mu_d ) \frac{1}{B} \delta(d,j) \right) = $$
$$-\frac{1}{B}(\sigma_d^2+\varepsilon)^{-3/2} \left( \sum \limits_{b=1}^B ( x_{bd} - \mu_d ) \delta(b,i) - \frac{1}{B} \sum \limits_{b=1}^B ( x_{bd} - \mu_d ) \right) \delta(d,j)$$

But notice that $\sum \limits_{b=1}^B ( x_{bk} - \mu_k ) = 0$ $\forall k$  by definition, since $\mu_k$ is exactly the mean of the $x_{bk}$ over the batch! So this simplifies to :

$$\frac{\partial (\sigma_d^2+\varepsilon)^{-1/2}}{\partial x_{ij}} = -\frac{1}{B}(\sigma_d^2+\varepsilon)^{-3/2} \left( \sum \limits_{b=1}^B ( x_{bd} - \mu_d ) \delta(b,i)  \right) \delta(d,j) =$$
$$-\frac{1}{B}(\sigma_d^2+\varepsilon)^{-3/2} (x_{id} - \mu_d) \delta(d,j)$$


Note that it seems we could simplify $(x_{id} - \mu_d)\delta(d,j)= x_{ij} - \mu_j$ but let's hold it for a moment to help us simplify the next expressions as well.. 

We can now collate everything and write 

$$\frac{\partial \hat x_{bd}}{\partial x_{ij}} = \frac{\partial (x_{bd} - \mu_d)(\sigma_d^2+\varepsilon)^{-1/2}}{\partial x_{ij}} = \frac{\partial (x_{bd} - \mu_d)}{\partial x_{ij}} \times (\sigma_d^2+\varepsilon)^{-1/2} + (x_{bd} - \mu_d) \times \frac{\partial (\sigma_d^2+\varepsilon)^{-1/2}}{\partial x_{ij}} = $$
$$ \left( \delta(b,i)\delta(d,j) - \frac{1}{B} \delta(d,j) \right) \times (\sigma_d^2+\varepsilon)^{-1/2} -\frac{1}{B} (x_{bd} - \mu_d) (\sigma_d^2+\varepsilon)^{-3/2} (x_{id} - \mu_d) \delta(d,j) = $$
$$ - \frac{1}{B} (\sigma_d^2+\varepsilon)^{-1/2} \left[ - B \delta(b,i)\delta(d,j) + \delta(d,j) + \frac{(x_{bd} - \mu_d)  (x_{id} - \mu_d)}{(\sigma_d^2+\varepsilon) }\delta(d,j) \right] $$

$$ - \frac{1}{B} (\sigma_d^2+\varepsilon)^{-1/2} \left[ 1 - B \delta(b,i) + \frac{(x_{bd} - \mu_d)  (x_{id} - \mu_d)}{(\sigma_d^2+\varepsilon) } \right]\delta(d,j) $$

And finally

$$\frac{\partial \mathcal{L}}{\partial x_{ij}} = \sum \limits_{b,d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \gamma_d \frac{\partial \hat x_{bd}}{\partial x_{ij}} =$$
$$\sum \limits_{b,d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \gamma_d \left\{ - \frac{1}{B} (\sigma_d^2+\varepsilon)^{-1/2} \left[ 1 - B \delta(b,i) + \frac{(x_{bd} - \mu_d)  (x_{id} - \mu_d)}{(\sigma_d^2+\varepsilon) } \right]\delta(d,j) \right\} =$$
$$- \frac{1}{B} \sum \limits_{b} \sum \limits_{d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \gamma_d (\sigma_d^2+\varepsilon)^{-1/2} \left[ 1 - B \delta(b,i) + \frac{(x_{bd} - \mu_d)  (x_{id} - \mu_d)}{(\sigma_d^2+\varepsilon) } \right]\delta(d,j) =$$
$$- \frac{1}{B} \sum \limits_{b} \frac{\partial \mathcal{L}}{\partial y_{bj}} \gamma_j (\sigma_j^2+\varepsilon)^{-1/2} \left[ 1 - B \delta(b,i) + \frac{(x_{bj} - \mu_j)  (x_{ij} - \mu_j)}{(\sigma_j^2+\varepsilon) } \right] =$$
$$- \frac{1}{B} \gamma_j (\sigma_j^2+\varepsilon)^{-1/2} \sum \limits_{b} \frac{\partial \mathcal{L}}{\partial y_{bj}} \left[ 1 - B \delta(b,i) + \frac{(x_{bj} - \mu_j)  (x_{ij} - \mu_j)}{(\sigma_j^2+\varepsilon) } \right] =$$
$$- \frac{1}{B} \gamma_j (\sigma_j^2+\varepsilon)^{-1/2} \left[ \sum \limits_{b} \frac{\partial \mathcal{L}}{\partial y_{bj}} - B \frac{\partial \mathcal{L}}{\partial y_{ij}} + \sum \limits_{b} \frac{\partial \mathcal{L}}{\partial y_{bj}} \frac{(x_{bj} - \mu_j)  (x_{ij} - \mu_j)}{(\sigma_j^2+\varepsilon) } \right] =$$
$$ \frac{ \gamma_j } {\sqrt{ \sigma_j^2+\varepsilon }}  \left( \frac{\partial \mathcal{L}}{\partial y_{ij}}-\frac{1}{B} \sum \limits_{b} \frac{\partial \mathcal{L}}{\partial y_{bj}} -\frac{1}{B} \frac{ x_{ij} - \mu_j}{\sigma_j^2+\varepsilon } \sum \limits_{b} \frac{\partial \mathcal{L}}{\partial y_{bj}} (x_{bj} - \mu_j) \right)$$

and we're done!

These were all the gradients that we needed! Note that it's **crucial** that we let gradients flow through the whole BN layer, including the normalization procedure, which results in this 'tying' of the inputs in the same batch!

You can check that indeed these reduce to the equations reported in the original paper, but the authors chose a slightly different route to get here, by computing explicitly the gradient wrt to $\mu_d$ and $\sigma^2_d$ and then computing $\frac{\partial \mathcal{L}}{\partial x_ij}$ by reusing the previous results. The solution is the same, theirs look a bt more compact maybe but overall $\mu_d$ and $\sigma^2_d$ are not *learned parameters* in our network, and so we don't explicitly need their gradient for each forward step, as said above, we only need to allow gradients to flow through them and drip down to the layers below!

To conclude, I'll report here all the equations we derived for posterity's sake! Good job!

$$ \frac{\partial \mathcal{L}}{\partial \beta_{j}} = \sum \limits_{b} \frac{\partial \mathcal{L}}{\partial y_{bj}}$$

$$\frac{\partial \mathcal{L}}{\partial \gamma_{j}} = \sum \limits_{b} \frac{\partial \mathcal{L}}{\partial y_{bj}} (x_{bj} - \mu_j)(\sigma_j^2+\varepsilon)^{-1/2}$$

$$\frac{\partial \mathcal{L}}{\partial \hat x_{ij}} = \frac{\partial \mathcal{L}}{\partial y_{ij}} \gamma_j$$


$$\frac{\partial \mathcal{L}}{\partial x_{ij}} = \frac{ \gamma_j } {\sqrt{ \sigma_j^2+\varepsilon }}  \left( \frac{\partial \mathcal{L}}{\partial y_{ij}}-\frac{1}{B} \sum \limits_{b} \frac{\partial \mathcal{L}}{\partial y_{bj}} -\frac{1}{B} \frac{ x_{ij} - \mu_j}{\sigma_j^2+\varepsilon } \sum \limits_{b} \frac{\partial \mathcal{L}}{\partial y_{bj}} (x_{bj} - \mu_j) \right)$$

## A note on broadcasting

One the concept people usually struggle with, while computing backprop, is **broadcasting**.

During the forward pass, especially in `code` (rather than in math) we [broadcast](https://jakevdp.github.io/PythonDataScienceHandbook/02.05-computation-on-arrays-broadcasting.html) tensors when we need to perform operations between tensors with different shapes; during the backward pass this results in the pesky sums that come out while computing the derivatives!

So why $\frac{\partial \mathcal{L}}{\partial \beta_{j}} = \sum \limits_{b,d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \times \frac{\partial y_{bd}}{\partial \beta_j}$ ?? Where does the sum and all these extra indeces come from?

If you're having trouble understanding this concept, I'm sure a few little illustrations will help!

First of all we need to agree that neural network modules can be thought of as computational graph. If you don't know what I'm talking about, maybe spend an hour of so skimming through [this resource](http://cs231n.github.io/optimization-2/) and/or its [corresponding video](https://www.youtube.com/watch?v=i94OvYb6noo). It's nothing too fancy, just a graphical way where we represent every operation in the module as a node in a graph, to visualize what's happening.

It's worth noting that these representations are so useful that behind the scenes (with some variation) this is what all the Deep Learning frameworks implement to keep track of computations done in order to implement the automatic differentiation we're so used to rely on!

Anyway, with that out of the way, I present you (credit for the image to [Frederik Kratzert](https://kratzert.github.io/2016/02/12/understanding-the-gradient-flow-through-the-batch-normalization-layer.html)) the computational graph for Batch Normalization!

<!-- <img src="BN_0.png" alt="BN_Graph" width="800"/> -->
![Computational Graph of Batch Normalization](BN_0.png){ width=500px }

<sub><sup>Note that Frederik explain the whole Batch Norm using the computational graph, so if you're interested go check-out his post! I'm just borrowing the framework to dive a bit more in depth into the backward pass of a broadcasted operation. </sup></sub>

In order to explain broadcasting, let's focus on the very end of the computational graph:

<!-- <img src="BN_1.jpg" alt="BN_Graph" width="600"/> -->
![Zoomed-in last part of the computational graph](BN_1.jpg ""){ width=400px }

In black we have object computed during the forward pass and in red the quantites that arise during the backward pass. I've annotated the dimensions in parenthesis (using tensor-like notation) to clarify a bit more.

<sup><sub>Sorry about switching to drawings, I'd rather spend time doing this write-up than using my touchpad to digitally draw the graph</sub></sup>

Let's actually focus on the very last operation in the graph, when we add the vector $\beta$ to the rescaled $(\hat x \gamma)$ to produce the final output $y$. In math language, this is expressed as $y_{bd} = \hat x_{bd} \gamma_d + \beta_d$ for $b = 1, \dots, B$ and $d = 1, \dots, D$.

In terms of the graph, this the zoomed-in version:

<!-- <img src="BN_2.jpg" alt="BN_Graph" width="600"/> -->
![Focus on the last operation and corresponding dimensions](BN_2.jpg){ width=400px }

I want to now point out a couple of thing. First, notice that $\beta$ has dimension $(D,)$ while $y$ has $(B,D)$. In code terms this is where the broadcasting happens; we add another dimension to the $\beta$ tensor so that it becomes $(1,D)$ and then we broadcast it to match the expected $(B,D)$ dimension to perform the elemet-wise addition. The tricky part is to understand that when we compute the backward pass, we will need the gradient with respect to $\beta$ to be a tensor of dimension $(D,)$ in order to properly perform our usual update $\beta = \beta - \alpha \frac{\partial \mathcal{L}}{\partial \beta}$!

The trick is in zooming-in even more and realize that even if vectorized operations are very useful both in code and math, and a little [Matrix Calculus](https://explained.ai/matrix-calculus/index.html) goes a long way in Deep Learning, when in doubt essentially all operations can be dumbed down to operations to single elements (aka scalars)!
This part of the graph I've chosen is particularly easy in that respect, as it's a (broadcasted) element-wise addition, and this is why I've chosen it as example.

So let's assume that we have a bath of 3 elements and only 2 units in the layer. This means that overall we'll have $3 \times 2$ $y_{bd}$ elements and only two values for $\beta = (\beta_1,\beta_2)$.

<!-- <img src="BN_3.jpg" alt="BN_Graph" width="400"/> -->
![Individual scalars exposed](BN_3.jpg){ width=400px }

The gradient flowing through each indivial $y_{bd}$ will contribute to the overall gradient of $\beta_1$.
To be a bit more precise I've also highlighted the fact that the elements $y_{b2}$ do not depend on $\beta_1$ and thus their contribute to the derivative $\frac{\partial \mathcal{L}}{\partial \beta_1}$ will be $0$!

You can see now where all the elements the sum in $\frac{\partial \mathcal{L}}{\partial \beta_{j}} = \sum \limits_{b,d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \times \frac{\partial y_{bd}}{\partial \beta_j}$ come from .. but still **why do we need to sum them** to compute the final gradient?

Well, in terms of computational graph the rule is usually **"gradients flowing into a node from different branches of the graph should be summed"**, and you can usually get away with using this plus trying to match tensor dimensions, but this is hardly a rigorous argument. To find a mathematically sound reason for it though we don't need to look further than the *chain rule*!

You're in fact doing nothing more complex than $$\frac{\partial f(a,b,c)}{\partial t} = \frac{\partial f(a,b,c)}{\partial a}\frac{\partial a}{\partial t} + \frac{\partial f(a,b,c)}{\partial b}\frac{\partial b}{\partial t} + \frac{\partial f(a,b,c)}{\partial c}\frac{\partial c}{\partial t} $$ where in our case $f(a,b,c)$ is the final matrix $y$, $a,b$ can be thought of as the intermediate results $\left(\hat x_{b1} \gamma_1 + \beta_1\right)$ for $b=1,2,3$ and $t$ of course represents $\beta_1$.

Now this expression should have become much more clear!

$$\frac{\partial \mathcal{L}}{\partial \beta_{j}} = \sum \limits_{b,d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \times \frac{\partial y_{bd}}{\partial \beta_j} = \sum \limits_{b,d} \frac{\partial \mathcal{L}}{\partial y_{bd}} \times \frac{\partial \left(\hat x_{bd} \gamma_d + \beta_d\right)}{\partial \beta_j} = \sum \limits_{b} \frac{\partial \mathcal{L}}{\partial y_{bd}}$$