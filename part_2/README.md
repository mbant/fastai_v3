## Fast.ai notebooks and code for Part 2

I've isolated part 2 because of the "library building" style, which is best kept separate from the more application-focused notebooks of Part 1.

This part is less interactive and more focused on writing code and infrastructure for a reimplementation of some parts of the fastai library, so it may be less interesting for others to go through and I strongly suggest you follow along with [the lessons](https://course.fast.ai/videos/?lesson=8) rather than skimming through my notebooks! For those who want to see my take on the code though, here it is.
In particular I tend to follow along with Jeremy for most of the interactive / dimension checking / plotting parts but re-implement from scratch all the `#export`ed cells.
